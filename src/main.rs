use std::{collections::HashSet, fmt::Write};

use arrayvec::ArrayString;
use ecow::{EcoString, EcoVec};
use reqwest::Client;
use serde::{de::Visitor, Deserialize, Serialize};

#[derive(Debug, Serialize, Deserialize)]
pub struct ADSBResponse {
    #[serde(rename = "ac")]
    aircraft: Vec<Aircraft>,
}

/// A datapoint corresponding to a detected aircraft
#[derive(Debug, Serialize, Deserialize)]
pub struct Aircraft {
    /// The 24-bit ICAO identifier of the aircraft
    hex: Address,
    /// The aircraft registration pulled from the database
    r: Option<EcoString>,
    /// The aircraft type pulled from the database
    t: Option<EcoString>,
    /// A bitfield for certain database flags
    ///
    /// military = dbFlags & 1
    /// interesting = dbFlags & 2
    /// PIA = dbFlags & 4
    /// LADD = dbFlags & 8
    //TODO: parse this
    #[serde(rename = "dbFlags")]
    db_flags: Option<u64>,
    /// Type of underlying messages/best source of current data for this position/aircraft
    #[serde(rename = "type")]
    ty: EcoString,
    /// Callsign, flight name, or aircraft registration
    flight: Option<EcoString>,
    /// Position of the aircraft
    #[serde(flatten)]
    pos: AircraftPosition,
    /// Aircraft barometric altitude in feet as a number or "ground" as a string
    alt_baro: Option<Altitude>,
    /// Geometric (GNSS/INS) altitude in feet referenced to WGS84 ellipsoid
    alt_geom: Option<f64>,
    /// Ground speed in knots
    gs: Option<f64>,
    /// Indicated air speed in knots
    ias: Option<f64>,
    /// True air speed in knots
    tas: Option<f64>,
    /// Mach number
    mach: Option<f64>,
    /// True track rate over ground in degrees (0-359)
    track: Option<f64>,
    /// Rate of change of track, degrees/second
    track_rate: Option<f64>,
    /// Roll, degrees, negative is left roll
    roll: Option<f64>,
    /// Heading, degrees clockwise from magnetic north
    mag_heading: Option<f64>,
    /// Heading, degrees clockwise from true north
    true_heading: Option<f64>,
    /// Rate of change of barometric altitude, feet/minute
    baro_rate: Option<f64>,
    /// Rate of change of geometric (GNSS/INS) altitude, feet/minute
    geom_rate: Option<f64>,
    /// Mode A code (Squawk) encoded as 4 octal digits
    squawk: Option<EcoString>,
    /// ADS-B emergency/priority status
    emergency: Option<Emergency>,
    /// Emitter category
    category: Option<Category>,
    /// Altimeter setting (QFE or QNH/QNE), hPa
    nav_qnh: Option<f64>,
    /// Selected altitude from the Mode Control Panel/Flight Control Unit (MCP/FCU) or equivalent equipment
    nav_altitude_mcp: Option<f64>,
    /// Selected altitude from the Flight Management System (FMS)
    nav_altitude_fms: Option<f64>,
    /// Selected heading
    nav_heading: Option<f64>,
    /// Set of engaged automation modes
    nav_modes: Option<EcoVec<EcoString>>,
    /// ADS-B version number
    version: Option<u8>,
    /// Navigation Integrity Category for Barometric Altitude
    nic_baro: Option<u8>,
    /// Navigation Accuracy for Position
    nac_p: Option<u8>,
    /// Navigation Accuracy for Velocity
    nac_v: Option<u8>,
    /// Source Integrity Level
    sil: Option<u8>,
    /// Interpretation of SIL
    sil_type: Option<EcoString>,
    /// Geometric Vertical Accuracy
    gva: Option<u8>,
    /// System Design Assurance
    sda: Option<u8>,
    /// List of fields derived from MLAT data
    mlat: Option<EcoVec<EcoString>>,
    /// List of fields derived from TIS-B data
    tisb: Option<EcoVec<EcoString>>,
    /// Total number of Mode S messages received from this aircraft
    messages: u64,
    /// How long ago (in seconds before "now") a message was last received from this aircraft
    seen: Option<f64>,
    /// Recent average RSSI in dbFS
    rssi: Option<f64>,
    /// Flight alert status bit
    alert: Option<u8>,
    /// Flight status special position identification bit
    spi: Option<u8>,
    /// Wind direction
    wd: Option<f64>,
    /// Wind speed
    ws: Option<f64>,
    /// Outer air temperature
    oat: Option<f64>,
    /// Static air temperature
    tat: Option<f64>,
    /// Last position of the aircraft
    last_pos: Option<AircraftPosition>,
    /// If no ADS-B or MLAT position available, a rough estimate of the latitude based on receiver's estimated co-ordinates
    rr_lat: Option<f64>,
    /// If no ADS-B or MLAT position available, a rough estimate of the longitude based on receiver's estimated co-ordinates
    rr_long: Option<f64>,
    #[serde(rename = "gpsOkBefore")]
    gps_ok_before: Option<f64>,
    #[serde(rename = "gpsOkLat")]
    gps_ok_lat: Option<f64>,
    #[serde(rename = "gpsOkLon")]
    gps_ok_lon: Option<f64>,
}

/// A type of underlying message
#[derive(Debug, Copy, Clone)]
pub enum MessageType {
    /// Messages from a Mode S or ADS-B transponder using 24-bit ICAO address
    AdsbIcao,
    /// Messages from an ADS-B equipped "non-transponder" emitter, e.g. a ground vehicle, using a 24-bit ICAO address
    AdsbIcaoNt,
    /// Rebroadcast of ADS-B messages originally sent via another data link, e.g. UAT, using a 24-bit ICAO address
    AdsrIcao,
    /// Traffic information about a non ADS-B target identified by a 24-bit ICAO address, e.g. a Mode S target tracked by secondary radar
    TisbIcao,
    /// ADS-C (received by monitoring satellite downlinks)
    Adsc,
    /// MLAT, position calculated arrival time differences using multiple receivers, outliers and varying accuracy is expected
    Mlat,
    /// Miscellaneous data received via a base station/SBS format, quality/source is unknown
    Other,
    /// Mode-S data from the plane's transponder (no position transmitted)
    ModeS,
    /// Messages from an ADS-B transponder using a non-ICAO address (e.g. anonymized address)
    AdsbOther,
    /// Rebroadcast of ADS-B messages originally sent via another data-link e.g. UAT using a non-ICAO address
    AdsrOther,
    /// Traffic information about a non-ADS-B target using a non-ICAO address
    TisbOther,
    /// Traffic information about a non-ADS-B target using a track/file identifier, typically from primary or Mode A/C radar
    TisbTrackfile,
}

/// ADS-B emergency/priority status
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash)]
pub enum Emergency {
    None,
    General,
    Lifeguard,
    Minfuel,
    Nordo,
    Unlawful,
    Downed,
    Reserved,
}

impl Emergency {
    /// Get this emergency/priority status as a string
    pub fn as_str(&self) -> &'static str {
        match self {
            Emergency::None => "none",
            Emergency::General => "general",
            Emergency::Lifeguard => "lifeguard",
            Emergency::Minfuel => "minfuel",
            Emergency::Nordo => "nordo",
            Emergency::Unlawful => "unlawful",
            Emergency::Downed => "downed",
            Emergency::Reserved => "reserved",
        }
    }

    /// Convert this emergency/priority status from a string
    ///
    /// Return `Err(())` if an invalid string is provided
    pub fn from_str(s: &str) -> Result<Emergency, ()> {
        match s {
            "none" => Ok(Emergency::None),
            "general" => Ok(Emergency::General),
            "lifeguard" => Ok(Emergency::Lifeguard),
            "minfuel" => Ok(Emergency::Minfuel),
            "nordo" => Ok(Emergency::Nordo),
            "unlawful" => Ok(Emergency::Unlawful),
            "downed" => Ok(Emergency::Downed),
            "reserved" => Ok(Emergency::Reserved),
            _ => Err(()),
        }
    }
}

impl Serialize for Emergency {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: serde::Serializer,
    {
        self.as_str().serialize(serializer)
    }
}

impl<'de> Deserialize<'de> for Emergency {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: serde::Deserializer<'de>,
    {
        struct EmergencyDeserializer;
        impl<'de> Visitor<'de> for EmergencyDeserializer {
            type Value = Emergency;

            fn expecting(&self, formatter: &mut std::fmt::Formatter) -> std::fmt::Result {
                formatter.write_str(
                    "one of none|general|lifeguard|minfuel|nordo|unlawful|downed|reserved",
                )
            }

            fn visit_str<E>(self, v: &str) -> Result<Self::Value, E>
            where
                E: serde::de::Error,
            {
                if let Ok(v) = Emergency::from_str(v) {
                    Ok(v)
                } else {
                    Err(E::custom(format!("invalid emergency type {v}")))
                }
            }
        }
        deserializer.deserialize_str(EmergencyDeserializer)
    }
}

/// ADS-B emitter category
#[derive(Debug, Copy, Clone)]
pub enum Category {
    /// No ADS-B emitter category information
    A0,
    /// Light (< 15500 lbs)
    A1,
    /// Small (15500 - 75000 lbs)
    A2,
    /// Large (75000 - 300000 lbs)
    A3,
    /// High vortex large (75000 - 300000 lbs, determined to generate a high wake vortex)
    A4,
    /// Heavy (> 300000 lbs)
    A5,
    /// High performance (>5g acceleration and 400 knots)
    A6,
    /// Rotorcraft
    A7,
    /// No ADS-B emitter category information
    B0,
    /// Glider/sailplane
    B1,
    /// Lighter-than-air
    B2,
    /// Parachutist/skydiver
    B3,
    /// Ultralight/hang-glider/paraglider
    B4,
    /// Reserved
    B5,
    /// UAV
    B6,
    /// Space/trans-atmospheric vehicle
    B7,
    /// No ADS-B emitter category information
    C0,
    /// Surface vehicle - emergency vehicle
    C1,
    /// Surface vehicle - service vehicle
    C2,
    /// Point obstacle (includes tethered balloons)
    C3,
    /// Cluster obstacle
    C4,
    /// Line obstacle
    C5,
    /// Reserved
    C6,
    /// Reserved
    C7,
}

impl Category {
    /// Get this category as a string
    pub fn as_str(&self) -> &'static str {
        match self {
            Category::A0 => "A0",
            Category::A1 => "A1",
            Category::A2 => "A2",
            Category::A3 => "A3",
            Category::A4 => "A4",
            Category::A5 => "A5",
            Category::A6 => "A6",
            Category::A7 => "A7",
            Category::B0 => "B0",
            Category::B1 => "B1",
            Category::B2 => "B2",
            Category::B3 => "B3",
            Category::B4 => "B4",
            Category::B5 => "B5",
            Category::B6 => "B6",
            Category::B7 => "B7",
            Category::C0 => "C0",
            Category::C1 => "C1",
            Category::C2 => "C2",
            Category::C3 => "C3",
            Category::C4 => "C4",
            Category::C5 => "C5",
            Category::C6 => "C6",
            Category::C7 => "C7",
        }
    }

    /// Convert this emergency/priority status from a string
    ///
    /// Return `Err(())` if an invalid string is provided
    pub fn from_str(s: &str) -> Result<Category, ()> {
        match s {
            "A0" => Ok(Category::A0),
            "A1" => Ok(Category::A1),
            "A2" => Ok(Category::A2),
            "A3" => Ok(Category::A3),
            "A4" => Ok(Category::A4),
            "A5" => Ok(Category::A5),
            "A6" => Ok(Category::A6),
            "A7" => Ok(Category::A7),
            "B0" => Ok(Category::B0),
            "B1" => Ok(Category::B1),
            "B2" => Ok(Category::B2),
            "B3" => Ok(Category::B3),
            "B4" => Ok(Category::B4),
            "B5" => Ok(Category::B5),
            "B6" => Ok(Category::B6),
            "B7" => Ok(Category::B7),
            "C0" => Ok(Category::C0),
            "C1" => Ok(Category::C1),
            "C2" => Ok(Category::C2),
            "C3" => Ok(Category::C3),
            "C4" => Ok(Category::C4),
            "C5" => Ok(Category::C5),
            "C6" => Ok(Category::C6),
            "C7" => Ok(Category::C7),
            _ => Err(()),
        }
    }
}

impl Serialize for Category {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: serde::Serializer,
    {
        self.as_str().serialize(serializer)
    }
}

impl<'de> Deserialize<'de> for Category {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: serde::Deserializer<'de>,
    {
        struct CategoryDeserializer;
        impl<'de> Visitor<'de> for CategoryDeserializer {
            type Value = Category;

            fn expecting(&self, formatter: &mut std::fmt::Formatter) -> std::fmt::Result {
                formatter.write_str("one of A0-C7")
            }

            fn visit_str<E>(self, v: &str) -> Result<Self::Value, E>
            where
                E: serde::de::Error,
            {
                if let Ok(v) = Category::from_str(v) {
                    Ok(v)
                } else {
                    Err(E::custom(format!("invalid category {v}")))
                }
            }
        }
        deserializer.deserialize_str(CategoryDeserializer)
    }
}

/// A sort of underlying message
#[derive(Debug, Copy, Clone)]
pub enum MessageSort {
    /// An ADS-B message
    Adsb,
    /// An ADS-C message
    Adsc,
    /// An ADS-R message
    Adsr,
    /// An MLAT message
    Mlat,
    /// A TISB message
    Tisb,
    /// Another message kind
    Other,
}

/// An aircraft position
#[derive(Debug, Copy, Clone, Serialize, Deserialize)]
pub struct AircraftPosition {
    /// Aircraft latitude in decimal degrees
    lat: Option<f64>,
    /// Aircraft longitude in decimal degrees
    lon: Option<f64>,
    /// Navigation Integrity Category
    nic: Option<u8>,
    /// Radius of Containment, meters
    rc: Option<f64>,
    /// How long ago (in seconds before "now") the position was last updated
    seen_pos: Option<f64>,
}

/// An aircraft address
///
/// This is a 24-bit ICAO-style address (represented as 6 hex digits)
///
/// If the high-bit is set, then this is a non-ICAO address (e.g. from TIS-B), and is represented starting with `~`
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash, Ord, PartialOrd)]
pub struct Address(u32);

/// An aircraft address
///
/// This is a 24-bit ICAO-style address (represented as 6 hex digits)
///
/// If the high-bit is set, then this is a non-ICAO address (e.g. from TIS-B), and is represented starting with `~`
pub enum AddressEnum {
    /// An ICAO address
    ICAO(ICAO),
    /// A non-ICAO address
    Other(Hex24),
}

/// A 24-bit address
pub struct Hex24(u32);

/// A 24-bit ICAO address
pub struct ICAO(u32);

impl ICAO {
    /// Get the raw 24-bit address
    pub fn addr(self) -> Hex24 {
        Hex24(self.0)
    }
}

impl Address {
    /// Get the raw 24-bit address
    pub fn addr(self) -> Hex24 {
        Hex24(self.0 & !(1 << 24))
    }

    /// Get this as an ICAO address, if it is one
    pub fn icao(self) -> Option<ICAO> {
        if self.is_icao() {
            Some(ICAO(self.0))
        } else {
            None
        }
    }

    /// Get whether this is an ICAO address
    pub fn is_icao(self) -> bool {
        self.0 & (1 << 24) == 0
    }
}

impl From<Address> for AddressEnum {
    fn from(value: Address) -> Self {
        if value.is_icao() {
            AddressEnum::ICAO(ICAO(value.0))
        } else {
            AddressEnum::Other(Hex24(value.0))
        }
    }
}

impl From<AddressEnum> for Address {
    fn from(value: AddressEnum) -> Self {
        match value {
            AddressEnum::ICAO(v) => Address(v.0),
            AddressEnum::Other(v) => Address(v.0 | 1 << 24),
        }
    }
}

impl Serialize for Address {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: serde::Serializer,
    {
        let mut buffer: ArrayString<16> = ArrayString::new();
        let masked = self.0 & !(1 << 24);
        if masked != self.0 {
            buffer.push('~')
        }
        write!(&mut buffer, "{masked:x}")
            .expect("buffer should have enough capacity for any 32-bit integer!");
        buffer.serialize(serializer)
    }
}

impl<'de> Deserialize<'de> for Address {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: serde::Deserializer<'de>,
    {
        struct AddressVisitor;

        impl<'de> Visitor<'de> for AddressVisitor {
            type Value = Address;

            fn expecting(&self, formatter: &mut std::fmt::Formatter) -> std::fmt::Result {
                formatter.write_str("a 24-bit address represented as 6 hex digits, starting with `~` for non-ICAO addresses, or as a 25-bit integer with the top bit set for non-ICAO addresses")
            }

            fn visit_i64<E>(self, v: i64) -> Result<Self::Value, E>
            where
                E: serde::de::Error,
            {
                if v > 1 << 25 {
                    return Err(E::custom("address has more than 24-bits + ICAO signifier"));
                }
                Ok(Address(v as u32))
            }

            fn visit_u64<E>(self, v: u64) -> Result<Self::Value, E>
            where
                E: serde::de::Error,
            {
                if v >= 1 << 25 {
                    return Err(E::custom("address has more than 24-bits + ICAO signifier"));
                }
                Ok(Address(v as u32))
            }

            fn visit_str<E>(self, v: &str) -> Result<Self::Value, E>
            where
                E: serde::de::Error,
            {
                let (icao, v) = if &v[0..1] == "~" {
                    (false, &v[1..])
                } else {
                    (true, v)
                };
                let addr = u32::from_str_radix(v, 16);
                let addr = match addr {
                    Ok(addr) if addr < 1 << 24 => addr,
                    _ => return Err(E::custom("invalid address")),
                };
                Ok(Address(addr | ((icao as u32) << 24)))
            }
        }

        deserializer.deserialize_str(AddressVisitor)
    }
}

#[derive(Debug, Copy, Clone)]
pub enum Altitude {
    Ground,
    Feet(f64),
}

impl Serialize for Altitude {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: serde::Serializer,
    {
        match self {
            Altitude::Ground => "ground".serialize(serializer),
            Altitude::Feet(ft) => ft.serialize(serializer),
        }
    }
}

impl<'de> Deserialize<'de> for Altitude {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: serde::Deserializer<'de>,
    {
        struct AltitudeDeserializer;

        impl<'de> Visitor<'de> for AltitudeDeserializer {
            type Value = Altitude;

            fn expecting(&self, formatter: &mut std::fmt::Formatter) -> std::fmt::Result {
                formatter.write_str("an altitude in feet or the string \"ground\"")
            }

            fn visit_i64<E>(self, v: i64) -> Result<Self::Value, E>
            where
                E: serde::de::Error,
            {
                self.visit_f64(v as f64)
            }

            fn visit_i128<E>(self, v: i128) -> Result<Self::Value, E>
            where
                E: serde::de::Error,
            {
                self.visit_f64(v as f64)
            }

            fn visit_u64<E>(self, v: u64) -> Result<Self::Value, E>
            where
                E: serde::de::Error,
            {
                self.visit_f64(v as f64)
            }

            fn visit_u128<E>(self, v: u128) -> Result<Self::Value, E>
            where
                E: serde::de::Error,
            {
                self.visit_f64(v as f64)
            }

            fn visit_f64<E>(self, v: f64) -> Result<Self::Value, E>
            where
                E: serde::de::Error,
            {
                Ok(Altitude::Feet(v))
            }

            fn visit_str<E>(self, v: &str) -> Result<Self::Value, E>
            where
                E: serde::de::Error,
            {
                if v == "ground" {
                    return Ok(Altitude::Ground);
                }
                Err(serde::de::Error::invalid_type(
                    serde::de::Unexpected::Str(v),
                    &self,
                ))
            }
        }

        deserializer.deserialize_any(AltitudeDeserializer)
    }
}

#[tokio::main]
async fn main() {
    let client = Client::new();
    let adsb_lol = client.get("https://api.adsb.lol/v2/mil").send();
    let adsb_one = client.get("https://api.adsb.one/v2/mil").send();

    let (adsb_lol_response, adsb_one_response) = tokio::join!(adsb_lol, adsb_one);

    let response = adsb_lol_response.unwrap();
    let body = response.text().await.unwrap();
    let lol_response = serde_json::from_str::<ADSBResponse>(&body).unwrap();
    println!("Found {} aircraft on adsb.lol", lol_response.aircraft.len());
    let no_flight_id = lol_response
        .aircraft
        .iter()
        .filter(|a| a.flight.is_none())
        .count();
    println!(
        "Aircraft w/out flight ID: {} ({:.1} %)",
        no_flight_id,
        100.0 * no_flight_id as f64 / lol_response.aircraft.len() as f64
    );
    let response = adsb_one_response.unwrap();
    let body = response.text().await.unwrap();
    let one_response = serde_json::from_str::<ADSBResponse>(&body).unwrap();
    println!("Found {} aircraft on adsb.one", one_response.aircraft.len());
    let no_flight_id = one_response
        .aircraft
        .iter()
        .filter(|a| a.flight.is_none())
        .count();
    println!(
        "Aircraft w/out flight ID: {} ({:.1} %)",
        no_flight_id,
        100.0 * no_flight_id as f64 / one_response.aircraft.len() as f64
    );

    let adsb_lol_set: HashSet<Address> =
        HashSet::from_iter(lol_response.aircraft.iter().map(|a| a.hex.clone()));
    let adsb_one_set: HashSet<Address> =
        HashSet::from_iter(one_response.aircraft.iter().map(|a| a.hex.clone()));

    let overlap_set: HashSet<Address> = HashSet::from_iter(
        adsb_lol_set
            .iter()
            .filter(|ac| adsb_one_set.contains(*ac))
            .cloned(),
    );

    println!(
        "Overlap: {} ==> Total aircraft: {}",
        overlap_set.len(),
        adsb_lol_set.len() + adsb_one_set.len() - overlap_set.len()
    );
}
